#!/usr/bin/env python3

import asyncio
import time

import aiohttp


async def pull_data(http_session: aiohttp.ClientSession,
                    pokemon_name: str) -> dict:
    """

    Parameters
    ----------
    http_session: aiohttp.ClientSession
        HTTP Session for requesting content.
    pokemon_name: str
        No need for explanation here.

    Returns
    -------
    dict
    """
    request_url = f"https://pokeapi.co/api/v2/pokemon/{pokemon_name}"
    resp_data = await http_session.request('GET', url=request_url)
    request_data = await resp_data.json()
    print(
        f"{pokemon_name}'s Base Experience: {request_data['base_experience']}")
    return request_data


async def main(pokemon_names: list):
    """
    The main attraction!

    Parameters
    ----------
    pokemon_names: list
        List of Pokemon names.

    """
    async with aiohttp.ClientSession() as req_sess:
        request_tasks = list()
        for best_friend in pokemon_names:
            request_tasks.append(pull_data(http_session=req_sess,
                                           pokemon_name=best_friend))
        final_data = await asyncio.gather(*request_tasks,
                                          return_exceptions=True)
        return final_data


if __name__ == "__main__":
    pokemons = list(range(1, 500))
    start_time = time.time()
    asyncio.run(main(pokemons))
    duration = time.time() - start_time
    print(f"Downloaded {len(pokemons)} sites in {duration} seconds")
