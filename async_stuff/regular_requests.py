#!/usr/bin/env python3
import time

import requests


def main(sites_list: list):
    for site in sites_list:
        print(requests.get(site).elapsed)


if __name__ == "__main__":
    sites = [
        "https://pokeapi.co/api/v2/pokemon/pikachu",
        "https://pokeapi.co/api/v2/pokemon/charmander",
        "https://pokeapi.co/api/v2/pokemon/onix",
        "https://pokeapi.co/api/v2/pokemon/ninetales"
    ] * 80
    start_time = time.time()
    main(sites)
    duration = time.time() - start_time
    print(f"Downloaded {len(sites)} sites in {duration} seconds")
