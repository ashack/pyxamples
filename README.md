# Pyxamples

Sometimes I just need to write small bits of code to better understand something
before I integrate it into a larger project. Or, I just want to learn something
new! So, this is where those things go.